package in.himtech.shedular.trigger;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;

public class MyJobListener implements JobListener{

	@Override
	public String getName() {
		System.out.println("Job Listener");
		return "Job_Listener";
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext context) {
		System.out.println("Inside JobExecutionVetoed method");
		
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext arg0) {
		System.out.println("Inside jobToBeExecuted method");
	}

	@Override
	public void jobWasExecuted(JobExecutionContext arg0, JobExecutionException arg1) {
		System.out.println("Inside jobWasExecuted method");
	}

}
