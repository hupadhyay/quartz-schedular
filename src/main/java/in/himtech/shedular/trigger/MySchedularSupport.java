package in.himtech.shedular.trigger;

import org.quartz.listeners.SchedulerListenerSupport;

public class MySchedularSupport extends SchedulerListenerSupport {
	
	@Override
	public void schedulerStarting() {
		System.out.println("SchedulerSupport --> schedulerStarting method");
	}
	
	@Override
	public void schedulerStarted() {
		System.out.println("SchedulerSupport --> schedulerStarted method");
	}
	
	@Override
	public void schedulerShuttingdown() {
		System.out.println("SchedulerSupport --> schedulerShuttingdown method");
	}
	
	
	@Override
	public void schedulerShutdown() {
		System.out.println("SchedulerSupport --> schedulerShutdown method");
	}

}
