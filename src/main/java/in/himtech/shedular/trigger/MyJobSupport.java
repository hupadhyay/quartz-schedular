package in.himtech.shedular.trigger;

import org.quartz.JobExecutionContext;
import org.quartz.listeners.JobListenerSupport;

public class MyJobSupport extends JobListenerSupport{

	@Override
	public String getName() {
		System.out.println("Job Support");
		return "Job_Support";
	}
	
	@Override
	public void jobToBeExecuted(JobExecutionContext arg0) {
		System.out.println("Inside jobToBeExecuted method from Support");
	}

}
