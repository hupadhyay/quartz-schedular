package in.himtech.shedular.trigger;

import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.listeners.TriggerListenerSupport;

public class MyTriggerSupport extends TriggerListenerSupport{

	@Override
	public String getName() {
		System.out.println("TriggerSupport --> getName method");
		return "Trigger_support";
	}

	@Override
	public void triggerFired(Trigger trigger, JobExecutionContext context) {
		System.out.println("TriggerSupport --> Inside triggeredFired method.");
	}
}
