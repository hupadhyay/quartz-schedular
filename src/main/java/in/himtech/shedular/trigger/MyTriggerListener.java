package in.himtech.shedular.trigger;

import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.Trigger.CompletedExecutionInstruction;
import org.quartz.TriggerListener;

public class MyTriggerListener implements TriggerListener{

	@Override
	public String getName() {
		System.out.println("TriggerListener --> getName()");
		return "Trigger_listener";
	}

	@Override
	public void triggerComplete(Trigger arg0, JobExecutionContext arg1, CompletedExecutionInstruction arg2) {
		System.out.println("TriggerListener --> Inside triggerComplete method");
	}

	@Override
	public void triggerFired(Trigger arg0, JobExecutionContext arg1) {
		System.out.println("TriggerListener --> Inside triggerFired method");
	}

	@Override
	public void triggerMisfired(Trigger arg0) {
		System.out.println("TriggerListener --> Inside triggerMisfired method");
	}

	@Override
	public boolean vetoJobExecution(Trigger arg0, JobExecutionContext arg1) {
		System.out.println("TriggerListener --> Inside vetoJobExecution method");
		return false;
	}

}
