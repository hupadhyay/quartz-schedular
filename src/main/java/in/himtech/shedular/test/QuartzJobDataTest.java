package in.himtech.shedular.test;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class QuartzJobDataTest {

	public static void main(String[] args) {
		try {
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			
			scheduler.start();
			
			//declare job
			JobDetail jobDetail = JobBuilder.newJob(HelloJobData.class)
					.withIdentity("HelloJob", "group1")
					.usingJobData("animal", "lion")
					.usingJobData("year", "1996")
					.build();
			
			//declare schedule
			SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
					.withIntervalInSeconds(3)
					.repeatForever();
			
			//declare trigger
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity("HelloTrigger", "group1")
					.startNow()
					.withSchedule(scheduleBuilder)
					.build();
			
			scheduler.scheduleJob(jobDetail, trigger);
			
//			scheduler.shutdown();
		} catch (SchedulerException e) {
			
			e.printStackTrace();
		}
		
		
	}
}