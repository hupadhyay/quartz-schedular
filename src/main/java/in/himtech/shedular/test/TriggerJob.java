package in.himtech.shedular.test;

import java.util.concurrent.TimeUnit;

import org.quartz.DateBuilder;
import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import in.himtech.shedular.trigger.MySchedularListener;
import in.himtech.shedular.trigger.MySchedularSupport;

public class TriggerJob {

	public static void main(String[] args) throws SchedulerException, InterruptedException {
		// declare job
		JobDetail jobDetail = JobBuilder.newJob(HelloJob.class).withIdentity("HelloJob", "group1").build();

		// declare trigger
		Trigger trigger = TriggerBuilder.newTrigger().withIdentity("HelloTrigger", "group1")
//				.startAt(new Date(System.currentTimeMillis() + 1000))
				.startAt(DateBuilder.futureDate(2, IntervalUnit.SECOND))
				.withSchedule(SimpleScheduleBuilder.simpleSchedule()
						.withIntervalInSeconds(3)
						.withRepeatCount(5))
				.build();

		Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

		scheduler.scheduleJob(jobDetail, trigger);
		
		// Adding of Schedular listener...
		scheduler.getListenerManager().addSchedulerListener(new MySchedularListener());
		scheduler.getListenerManager().addSchedulerListener(new MySchedularSupport());

		scheduler.start();

		TimeUnit.SECONDS.sleep(20);

		scheduler.shutdown();
	}
}
