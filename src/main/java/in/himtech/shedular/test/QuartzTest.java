package in.himtech.shedular.test;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import in.himtech.shedular.trigger.MyJobListener;
import in.himtech.shedular.trigger.MyJobSupport;
import in.himtech.shedular.trigger.MyTriggerListener;
import in.himtech.shedular.trigger.MyTriggerSupport;

public class QuartzTest {

	public static void main(String[] args) {
		try {
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			
			scheduler.start();
			
			//declare job
			JobDetail jobDetail = JobBuilder.newJob(HelloJob.class)
					.withIdentity("HelloJob", "group1")
					.build();
			
			//declare schedule
			SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
					.withIntervalInSeconds(30)
					.repeatForever();
			
			//declare trigger
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity("HelloTrigger", "group1")
					.startNow()
					.withSchedule(scheduleBuilder)
					.build();
			
			scheduler.scheduleJob(jobDetail, trigger);
			
			// Adding of JobListeners...
			scheduler.getListenerManager().addJobListener(new MyJobListener());
			scheduler.getListenerManager().addJobListener(new MyJobSupport());
			
			// Adding of Trigger Listeners ....
			scheduler.getListenerManager().addTriggerListener(new MyTriggerListener());
			scheduler.getListenerManager().addTriggerListener(new MyTriggerSupport());
			
//			scheduler.shutdown();
		} catch (SchedulerException e) {
			
			e.printStackTrace();
		}
		
		
	}
}