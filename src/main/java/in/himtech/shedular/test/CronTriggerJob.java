package in.himtech.shedular.test;

import java.time.ZoneId;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class CronTriggerJob {

	public static void main(String[] args) throws SchedulerException, InterruptedException {
		// declare job
		JobDetail jobDetail = JobBuilder.newJob(HelloJob.class).withIdentity("HelloJob", "group1").build();

		// declare trigger
		Trigger trigger = TriggerBuilder.newTrigger().withIdentity("HelloTrigger", "group1")
//				.startAt(new Date(System.currentTimeMillis() + 1000))
//				.startAt(DateBuilder.futureDate(2, IntervalUnit.SECOND))
				.withSchedule(CronScheduleBuilder.cronSchedule("0 0/2 * * * ?")
						.inTimeZone(TimeZone.getTimeZone(ZoneId.systemDefault()))
						.withMisfireHandlingInstructionFireAndProceed())
				
				.build();

		Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

		scheduler.scheduleJob(jobDetail, trigger);

		scheduler.start();

		TimeUnit.MINUTES.sleep(20);

		scheduler.shutdown();
	}
}
