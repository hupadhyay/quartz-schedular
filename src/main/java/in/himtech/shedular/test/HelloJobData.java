package in.himtech.shedular.test;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class HelloJobData implements Job{

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println("Hello Quartz Schedular!!!");
		
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		String animal = jobDataMap.getString("animal");
		int year = jobDataMap.getInt("year");
		
		System.out.printf("I saw a %s in %d\n", animal, year);
	}
	
}