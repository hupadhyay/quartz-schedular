package in.himtech.shedular.cluster;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

public class SchedularTest {
	public static void main(String[] args) throws SchedulerException {
		SchedulerFactory sf = new StdSchedulerFactory();
		Scheduler sched = sf.getScheduler();

		String schedId = sched.getSchedulerInstanceId();
		System.out.println("ScheduledJobId: " + schedId);

		sched.start();
		try {
			Thread.sleep(3600L * 1000L);
		} catch (Exception e) {
			//
		}
		
		sched.shutdown();
	}
}
