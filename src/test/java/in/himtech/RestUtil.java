package in.himtech;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

public class RestUtil {

	public static String fetchToken(){
		Client client = ClientBuilder.newClient(new ClientConfig().register(String.class));
		WebTarget webTarget = client.target("http://tracking-1-qa.private.run.covisintrnd.com/trackings/3390a51e-c023-4a74-a01d-dbd51b8340e3");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON)
				.accept("application/vnd.com.covisint.platform.messaging.tracking.v1+json")
				.header("Content-Type", "application/vnd.com.covisint.platform.messaging.tracking.v1+json")
				.header("x-realm", "IOT_SOL_LOCAL").header("x-requestor", "hku").header("x-requestor-app", "Postman");
		Response response = invocationBuilder.get();

		String strResponse = response.readEntity(String.class);

		return strResponse;

	}
	
	
	public static void main(String[] args) {
		String str = fetchToken();
		System.out.println(str);
	}
	
}
